#!/bin/bash

#=================================================
# GENERIC START
#=================================================
# IMPORT GENERIC HELPERS
#=================================================

source /usr/share/yunohost/helpers

# Exit if an error occurs during the execution of the script
ynh_abort_if_errors

# Load common variables and helpers
source ./_common.sh

#=================================================
# RETRIEVE ARGUMENTS
#=================================================

# Retrieve arguments
domain=$YNH_APP_ARG_DOMAIN
path_url=$YNH_APP_ARG_PATH
is_public=$YNH_APP_ARG_IS_PUBLIC
only_use_ldap=$YNH_APP_ARG_IS_PUBLIC
admin=$YNH_APP_ARG_ADMIN

#=================================================
# REGISTER DOMAIN
#=================================================

# Normalize the url path syntax
path_url=$(ynh_normalize_url_path $path_url)

# This function check also the availability of this one
ynh_webpath_register $app $domain $path_url

#=================================================
# REGISTER DOMAIN
#=================================================

# Check user parameter
ynh_user_exists "$admin" \
  || ynh_die "The chosen admin user does not exist."

#=================================================
# STORE SETTINGS FROM MANIFEST
#=================================================

ynh_app_setting_set $app admin $admin
ynh_app_setting_set $app path_url $path_url
ynh_app_setting_set $app is_public $is_public
ynh_app_setting_set $app only_use_ldap $only_use_ldap

#=================================================
# STANDARD MODIFICATIONS
#=================================================
# FIND AND OPEN PORTS
#=================================================

# Find free ports
port=$(ynh_find_port 8080)
portUnicorn=$(ynh_find_port 9080)

yunohost firewall allow --no-upnp TCP $port 2>&1
yunohost firewall allow --no-upnp TCP $portUnicorn 2>&1
ynh_app_setting_set $app web_port $port
ynh_app_setting_set $app unicorn_port $portUnicorn

#=================================================
# NGINX CONFIGURATION
#=================================================

# Create a dedicated nginx config
ynh_add_nginx_config

#=================================================
# CONFIGURE GITLAB
#=================================================

# Configure gitlab with gitlab.rb file
config_gitlab

#=================================================
# INSTALL DEPENDENCIES
#=================================================

ynh_install_app_dependencies openssh-server

#=================================================
# DOWNLOAD, CHECK AND INSTALL GITLAB
#=================================================

setup_source $architecture

#=================================================
# SPECIFIC SETUP
#=================================================
# GETTING ADMIN INFO AND ADD AS A GITLAB USER AND CONFIGURE SIGN IN SYSTEM
#=================================================

mailadmin=$(ynh_user_get_info $admin mail)
rdmPass=$(ynh_string_random 30)

echo "newuser = User.new({ \"email\"=>'$mailadmin', \"username\"=>'$admin', \"name\"=>'$admin', \"password\"=>'$rdmPass'})
newuser.admin = true
newuser.confirmed_at = Time.now
newuser.confirmation_token = nil
newuser.save
ApplicationSetting.last.update_attributes(password_authentication_enabled_for_web: $only_use_ldap, signup_enabled: $only_use_ldap)"  | sudo gitlab-rails console

#=================================================
# RESTART TO TAKE INTO ACCOUNT CHANGES
#=================================================

sudo gitlab-ctl reconfigure

#=================================================
# SETUP SSOWAT
#=================================================

# If app is public, add url to SSOWat conf as skipped_uris
if [[ $is_public -eq 1 ]]; then
  # unprotected_uris allows SSO credentials to be passed anyway.
  ynh_app_setting_set "$app" unprotected_uris "/"
fi

#=================================================
# GENERIC FINALISATION
#=================================================
# RELOAD NGINX
#=================================================

service nginx reload

#=================================================
# SETUP LOGROTATE
#=================================================

# Configure logrotate
#ynh_use_logrotate "/var/log/$app"
